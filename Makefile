files = main.c tree.c tree.h node.c node.h

all:
	clear
	gcc -Wall $(files)

debug:
	clear
	gcc -Wall -g $(files)
