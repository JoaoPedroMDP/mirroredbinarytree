#ifndef __NODE__
#define __NODE__

typedef struct Node Node;
struct Node
{
    int data;
    Node *left;
    Node *right;
};

Node *createNode(int data);
void printNode(Node *node);
#endif