#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tree.h"

#define TEST_SIZE 7
#define NUMBERS_DELTA 50
void generateTree(int treeSize, Tree *tree){
    int i;
//    int elements[TEST_SIZE] = {3,2,1,6,5,7,9}; // PARA TESTES CONTROLADOS
    srand(time(NULL)); // PARA TESTES ALEATÓRIOS
    for(i = 0; i < treeSize; i++){
        Node *newNode = createNode(rand() % NUMBERS_DELTA); // PARA TESTES ALEATÓRIOS
//        Node *newNode = createNode(elements[i]); // PARA TESTES CONTROLADOS
        insertNode(tree, newNode, 0);
    }
}

int main(){
    Tree *tree = createTree();
    generateTree(TEST_SIZE, tree);
    printTree(tree->root);
    printf("\n");

//    TESTAR ESPELHAMENTO
//    Tree *mirrored = createTree();
//    mirrorTree(mirrored, tree->root);
//    printTree(mirrored->root);
//    printf("\n");
//    printf("%d", isMirrored(tree->root, mirrored->root));

//    TESTAR REMOÇÃO
//    removeNode(NULL, tree->root, 6);
//    printTree(tree->root);
//    printf("\n");
//    removeNode(NULL, tree->root, 1);
//    printTree(tree->root);
//    printf("\n");
//    removeNode(NULL, tree->root, 7);
//    printTree(tree->root);
    return 0;
}
