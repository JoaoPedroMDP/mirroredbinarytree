#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "node.h"

Node *mallocNode()
{
    Node *newNode = malloc(sizeof(Node));
    newNode->data = 0;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

Node *createNode(int data)
{
    Node *newNode = mallocNode();
    newNode->data = data;
    return newNode;
}

void printNode(Node *node)
{
    printf("%d]", node->data);
}
