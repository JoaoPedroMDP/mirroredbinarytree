#ifndef __TREE__
#define __TREE__
#include "node.h"

typedef struct Tree Tree;
struct Tree{
    int size;
    Node *root;
};

Tree* mallocTree();
Tree *createTree();
void insertNode(Tree *tree, Node *newNode, int reversed);
void printTree(Node *node);
void mirrorTree(Tree *mirroredTree, Node *original);
int isMirrored(Node *original, Node *mirrored);
Node* removeNode(Node* parent, Node* node, int toRemove);
#endif
