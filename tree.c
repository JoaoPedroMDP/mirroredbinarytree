#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
Tree* mallocTree(){
    Tree *newTree = (Tree*) malloc(sizeof(Tree));
    newTree->size = 0;
    newTree->root = NULL;
    return newTree;
}

Tree* createTree(){
    return mallocTree();
}

int toLeft(int treeData, int newNodeData, int reversed){
    if(reversed){
        return !(newNodeData < treeData);
    }else{
        return newNodeData < treeData;
    }
}

void insertNode(Tree *tree, Node *newNode, int reversed){
    if(tree->root == NULL){
        tree->root = newNode;
        tree->size++;
        return;
    }

    Node *current = tree->root;
    int foundCorrectParent = 0;
    while(foundCorrectParent == 0){
        if(toLeft(current->data, newNode->data, reversed)){
            if(current->left == NULL){
                current->left = newNode;
                tree->size++;
                foundCorrectParent = 1;
            }else{
                current = current->left;
            }
        }else if(!toLeft(current->data, newNode->data, reversed)){ // nao quis criar outra função, então só inverti a de esquerda
            if(current->right == NULL){
                current->right = newNode;
                tree->size++;
                foundCorrectParent = 1;
            }else{
                current = current->right;
            }
        }else{
            // sem repetições, pô
            foundCorrectParent = 1;
        }
    }
}

void printTree(Node *node){
    if(node != NULL){
        printNode(node);

        if(node->left != NULL){
            printf("[<");
            printTree(node->left);
            printf("^");
        }
        if(node->right != NULL){
            printf("[>");
            printTree(node->right);
            printf("^");
        }
    }
}

void mirrorTree(Tree *mirroredTree, Node *original){
    if(original != NULL){
        Node *newNode = createNode(original->data);
        insertNode(mirroredTree, newNode, 1);

        if(original->left != NULL){
            mirrorTree(mirroredTree, original->left);
        }
        if(original->right != NULL){
            mirrorTree(mirroredTree, original->right);
        }
    }
}

int isMirrored(Node *original, Node *mirrored){
    int leftIsMirrored = 1, rightIsMirrored = 1;
    if(original == NULL){
        return mirrored == NULL;
    }

    if(original->data == mirrored->data)
    {
        if(original->left != NULL){
            if(mirrored ->right != NULL){
                leftIsMirrored = isMirrored(original->left, mirrored->right);
            }
        }else{
            leftIsMirrored = mirrored->right == NULL;
        }

        if(original->right != NULL){
            if(mirrored->left != NULL){
                leftIsMirrored = isMirrored(original->right, mirrored->left);
            }
        }else{
            leftIsMirrored = mirrored->left == NULL;
        }

        return leftIsMirrored && rightIsMirrored;
    }else{
        return 0;
    }
}

void removeLeaf(Node *parent, Node *node){
    if(parent->left->data == node->data){
        parent->left = NULL;
    }else{
        parent->right = NULL;
    }

    free(node);
}

void removeElbow(Node *parent, Node *node){
    if(parent->left == node){
        if(node->left != NULL){
            parent->left = node->left;
        }else{
            parent->left = node->right;
        }
    }else{
        if(node->left != NULL){
            parent->right = node->left;
        }else{
            parent->right = node->right;
        }
    }
    free(node);
}

Node* getBiggestBefore(Node *node){
    Node *walker = node->left;

    while(walker !=NULL){
        if(walker->right == NULL){
            return walker;
        }else{
            walker = walker->right;
        }
    }
}

void removeComplex(Node *parent, Node *node){
    Node *biggestBefore = getBiggestBefore(node);
    node->data = biggestBefore->data;
    removeLeaf(node, biggestBefore);
}

Node* removeNode(Node* parent, Node* node, int toRemove){
    if(toRemove < node->data){
        removeNode(node, node->left, toRemove);
    }else if(toRemove > node->data){
        removeNode(node, node->right, toRemove);
    }else{
        // Encontrou o nó a ser removido
        if(node->left == NULL){
            if(node->right == NULL){
                removeLeaf(parent, node);
            }else{
                removeElbow(parent, node);
            }
        }else{
            if(node->right == NULL){
                removeElbow(parent, node);
            }else{
                removeComplex(parent, node);
            }
        }
    }
}